# 在线开放教材


* [American Institute of Mathematics Open Textbook](https://aimath.org/textbooks/approved-textbooks/)
* [Active Calculus](http://faculty.gvsu.edu/boelkinm/Home/Active_Calculus.html)
* [A Friendly Introduction to Mathematical Logic](http://minerva.geneseo.edu/a-friendly-introduction-to-mathematical-logic/)
* [Sage for Undergraduates](http://www.gregorybard.com/books.html)